# /usr/local/bin/python3

from random import randint, shuffle, seed

def get_int_list(num_pairs, num_unpaired, int_max=1000, seed_val=None):
	if seed_val != None:
		# optionally use a seed for repeatable results
		seed(seed_val)
	l = [randint(0,int_max) for _ in range(num_pairs)]
	l.extend(l) # double up all the paired numbers
	l.extend([randint(0,int_max) for _ in range(num_unpaired)]) # add the unpaired numbers
	shuffle(l)
	return l