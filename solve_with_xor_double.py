from list_gen import get_int_list

def get_left_most_one_bit_mask(x):
	mask = 1
	while mask & x == 0:
		mask *= 2 
	return mask

def split_on_mask(mask, l):
	return filter(lambda x: mask & x == 0, l)

def xor_sum(l):
	return reduce(lambda x, y: x^y, l)

def solve(l):
	xor = xor_sum(l)
	half_list = split_on_mask(get_left_most_one_bit_mask(xor), l)
	print half_list
	first_unrepeated = xor_sum(half_list)
	second_unrepeated = xor ^ first_unrepeated
	return [first_unrepeated, second_unrepeated]

def main():
	l = get_int_list(10,2,int_max=100,seed_val=2)
	print l
	print solve(l)

import unittest
class Test(unittest.TestCase):

	def test_mask(self):
		self.assertEqual(4, get_left_most_one_bit_mask(4))
	def test_mask_2(self):
		self.assertEqual(4, get_left_most_one_bit_mask(20))

if __name__ == "__main__":
	#unittest.main()
	main()