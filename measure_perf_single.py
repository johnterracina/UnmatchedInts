import matplotlib.pyplot as plt
from list_gen import get_int_list 
from time import time
import solve_with_sort
import solve_with_xor_single


def time_it(f, arg):
	start_time = time()
	result = f(arg)
	time_taken = time() - start_time
	return [result, time_taken]

test_sizes =[2**x for x in range(4,20)]
tests = [get_int_list(x,1) for x in test_sizes]

xor_results = []
sort_results = []

for test in tests:
	print len(test)
	sort_results.append(time_it(solve_with_sort.solve, test) + [len(test)])
	xor_results.append(time_it(solve_with_xor_single.solve, test) + [len(test)])


xor_results = zip(*xor_results)
sort_results = zip(*sort_results)

xor_plot, = plt.plot(xor_results[2], xor_results[1], label='xor', marker='x')
sort_plot, = plt.plot(sort_results[2], sort_results[1], label='sort',marker='o')
plt.legend(handles=[xor_plot, sort_plot])
#plt.yscale('log')
#plt.xscale('log')
plt.ylabel('time')
plt.xlabel('list size')
plt.show()



