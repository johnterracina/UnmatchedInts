from list_gen import get_int_list

def solve(l):
	return reduce(lambda x, y: x^y, l)

def main():
	l = get_int_list(10,1,int_max=10,seed_val=1)
	print l
	print solve(l)

if __name__ == "__main__":
	main()