from list_gen import get_int_list

def solve(l):
	l.sort()
	un_paried = []
	i = 0 
	while i < len(l):
		if i != len(l)-1 and l[i] == l[i+1]:
			i += 1 # skip testing the second item in the pair
		else:
			un_paried.append(l[i])
		i += 1

	return un_paried


def main():
	l = get_int_list(10,1,int_max=10,seed_val=1)
	print l
	print solve(l)

if __name__ == "__main__":
	main()